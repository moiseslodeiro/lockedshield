<?php


/*-------------------------------------
 Basic Configuration
-------------------------------------*/ 

	$allowSync = false;

/*-------------------------------------
 Database configuration
--------------------------------------*/

	$dbHost = 'localhost';
	$dbUsername = 'root';
	$dbPassWord = '';
	$dbDataBase = 'lockedshield';


//-------------------------------------


//-------------------------------------
// If you edit the code below, it's 
// under your responsibility
//-------------------------------------

	//--[ PHP Headers ]------------------------------------
		ob_start();

		date_default_timezone_set('UTC'); 
		header('Access-Control-Allow-Origin: *');
		require_once( 'Crypt/RSA.php' );
		session_start();

	//-----------------------------------------------------


	//--[ Database Server Connection ]---------------------

		$connectionId = mysql_connect( $dbHost , $dbUsername , $dbPassWord ) or exit( 'Connection Error' );
		mysql_select_db( $dbDataBase , $connectionId );

	//-----------------------------------------------------
	
	$do = @$_POST['action'];
	switch( $do ){

		case 'sayHi' : newUser() ; break;
		case 'registerUid': newUser2(); break;
		case 'checkLogin' : checkLogin(); break;
		case 'confirmDevice' : confirmDevice(); break;
		case 'updateCode': updateCode(); break;
		case 'checkCode' : checkCode(); break;
		case 'verifyAccount': verifyAccount(); break;
		case 'savePassword': savePassword(); break;
		case 'updateDevices': updateDevices(); break;
		case 'syncDevices' : syncDevices(); break;
		case 'getStatus' : getStatus(); break;
		case 'getPasswords': getPasswords(); break;
		default: echo 'Unknown Instruction'; break;

	}


	/**
	 * Removes any non-alphanumeric characters.
	 *
	 * @param string $string String to sanitize
	 * @param array $allowed An array of additional characters that are not to be removed.
	 * @return string Sanitized string
	 * @author CakePHP.org
	 */

	function cleanString( $string , $allowed = array() ) 
	{
		
		$allow = null;

		if( !empty( $allowed )) 
			foreach( $allowed as $value )
				$allow .= "\\$value";
	
		if( !is_array( $string ))
			return preg_replace( "/[^{$allow}a-zA-Z0-9]/" , '' , $string );

		$cleaned = array();
		foreach( $string as $key => $clean ){
			$cleaned[ $key ] = preg_replace( "/[^{$allow}a-zA-Z0-9]/s" , '' , $clean );		
		}

		return $cleaned;

	}


	function getRaw( $parseData , $paramName , $paramValue , $error = '' , $fields = '' , $conditions = '' ){

		global $connectionId;

		if( $fields == '*' )
			$select = '*';
		else
			$select = 'lck_private';

		$query = trim( 'SELECT '.$select.(( $fields != '' )?( ','.$fields ):( '' )).' FROM lck_devices WHERE `'.$paramName.'` = "'.$paramValue.'" '.$conditions );
		$query = mysql_query( $query , $connectionId );

		if( mysql_num_rows( $query ) == 0 )
			exit( $error.'_01' );
		else
			$priv = mysql_fetch_assoc( $query );

		$dataQuery = '';
		if( trim( $fields ) != '' ){

			$dataQuery = $priv;
			unset( $dataQuery['lck_private'] );

		}

		$priv = $priv['lck_private'];
		$priv = str_replace( '-----BEGIN RSA PRIVATE KEY-----' , '' , $priv );
		$priv = str_replace( '-----END RSA PRIVATE KEY-----'   , '' , $priv );

		$data = explode( ',' , $parseData );



		$rsa = new Crypt_RSA();
		$rsa->setEncryptionMode( CRYPT_RSA_ENCRYPTION_PKCS1 );
		$rsa->loadKey( $priv );
		
		$decrypted = '';

		for( $i = 0 ; $i < count( $data ) ; $i++ ){
			
			if( $data[$i] == 'false' )
				exit('getRaw_01');

			$decrypted .= $rsa->decrypt( base64_decode( $data[$i] ));
		}


		if( !is_object( json_decode( $decrypted ) ) )
		{

			if( $fields == '' )
				return $decrypted;
			else
				return array( $decrypted , $dataQuery );

		}


		$data = json_decode( $decrypted , true );

		$allowChars = array( '[' , ']' , '+' , '/' , '\\' , '-' , '*' ,
							 '=' , '{' , '}' , '"' , "'" , ' ' , "\n" , 
							 "\r" , '^' , '!' , '|' , '@' , '#' , '$' , 
							 '%' , '&' , '(' , ')' , '?' , ':' , ';'  ,
							  ',' , '.' , '_' , '´' , '`'
						);

		$data = cleanString( $data , $allowChars );

		if( trim( $fields ) != '' )
			return array( $data , $dataQuery );
		else
			return $data;

	}


	function sliceString( $text , $key )
	{


		$numSlices = ceil( strlen( $text )/50 );

		if( $numSlices <= 0 )
			exit();

		$slices = array();

		$rsa = new Crypt_RSA();
		$rsa->setEncryptionMode( CRYPT_RSA_ENCRYPTION_PKCS1 );
		$rsa->loadKey( $key );

		for( $i = 0 ; $i < $numSlices ; $i++ )
			$slices[$i] = base64_encode( $rsa->encrypt( substr( $text , $i*50 , 50 ) ) );
		
		return json_encode( $slices );
		
	}


	//----------------------------------------------------------------------------
	//----------------------------------------------------------------------------
	//----------------------------------------------------------------------------	


	function updateDevices(){
		
		$data = getRaw( $_POST['data'] , 'lck_sessid' , $_POST['sessid'] , 'updateDevices' , 'lck_uid,lck_public' );

		if( md5( $_POST['sessid'] ) == $data[0]['session'] && $data[0]['locpubl'] == md5( $data[1]['lck_public'] ) ){

			$uid = $data[1]['lck_uid'];
			$pub = $data[1]['lck_public'];
			$dev = $data[0]['device'];
			$sta = (( $data[0]['status'] == 'on' )?('1'):('0'));
		

			global $connectionId;
			$query = mysql_query( 'UPDATE lck_devices SET lck_active = "'.$sta.'" WHERE lck_uid = "'.$uid.'" AND lck_dev = "'.$dev.'"' , $connectionId );

			if( mysql_affected_rows( $connectionId ) == 1 )
				echo 'ok';
			else
				echo 'updateDevices_03';

		}else{
			echo 'updateDevices_02';
		}

	}


	function getStatus(){
		
		$data = getRaw( $_POST['data'] , 'lck_sessid' , $_POST['sessid'] , 'getStatus' , 'lck_uid,lck_public' );

		if( md5( $_POST['sessid'] ) == $data[0]['session'] && $data[0]['locpubl'] == md5( $data[1]['lck_public'] ) ){

			$uid = $data[1]['lck_uid'];
			global $connectionId;

			$q = mysql_fetch_array( mysql_query( 'SELECT lck_active FROM lck_devices WHERE lck_uid = "'.$uid.'" AND MD5(lck_sessid) = "'.$data[0]['session'].'"' , $connectionId ));

			if( $q['lck_active'] == 0 )
				echo 'logout';
	
		}else{
			echo 'getStatus_02';
		}



	}


	function syncDevices(){

		$data = getRaw( $_POST['data'] , 'lck_sessid' , $_POST['sessid'] , 'syncDevices' , 'lck_uid,lck_public' );

		if( md5( $_POST['sessid'] ) == $data[0]['session'] && $data[0]['locpubl'] == md5( $data[1]['lck_public'] ) ){

			$uid  = $data[1]['lck_uid'];
			$pub  = $data[1]['lck_public'];

			$query = 'SELECT lck_dev,lck_name,lck_active FROM lck_devices WHERE lck_uid = "'.$uid.'"';
			global $connectionId;
			$query = mysql_query( $query , $connectionId );

			if( mysql_num_rows( $query ) == 0 )
				echo 'syncDevices_03';
			else
			{

				$devs = array();

				while( $data = mysql_fetch_array( $query ))
					$devs[$data['lck_dev']] = json_encode( array( 'name_' => $data['lck_name'] , 'active' => $data['lck_active'] ));

				echo sliceString( json_encode( $devs ) , $pub );

			}

		}else{

			echo 'syncDevices_02';

		}


	}


	function getPasswords(){



		$data = getRaw( $_POST['data'] , 'lck_sessid' , $_POST['sessid'] , 'getPassword' , 'lck_uid,lck_public' );

		if( md5( $_POST['sessid'] ) == $data[0]['session'] && $data[0]['locpubl'] == md5( $data[1]['lck_public'] ) ){

			$uid  = $data[1]['lck_uid'];
			$pub  = $data[1]['lck_public'];

			$query = 'SELECT GROUP_CONCAT( lck_data SEPARATOR \';\' ) passwords FROM lck_strongbox WHERE lck_uid = "'.$uid.'"';
			$query = mysql_query( $query );

			if( mysql_num_rows( $query ) == 0 )
				echo 'getPasswords_03';
			else
			{
				$data = mysql_fetch_array( $query );

				if( trim( $data['passwords'] ) == '' )
					echo 'cleanPasswords';

				$data = $data['passwords'];
				echo sliceString( $data , $pub );
			}


		}else{

			echo 'getPasswords_02';

		}


	}



	function savePassword(){

		$data = getRaw( $_POST['data'] , 'lck_sessid' , $_POST['sessid'] , 'savePassword' , 'lck_uid' );
		
		$password = $data[0];
		$uid = $data[1]['lck_uid'];
		unset( $data );

		$dbHash = mysql_fetch_array( mysql_query( 'SELECT GROUP_CONCAT( lck_data SEPARATOR \';\' ) dbHash FROM lck_strongbox WHERE lck_uid = "'.$uid.'"' ));

		if( @( $_POST['dbHash'] != '' ) && @( $_POST['dbHash'] != md5( $dbHash['dbHash'] )))
			exit('syncIt');

		global $connectionId;
		$uniqid = uniqid();

		$query = 'INSERT INTO `lck_strongbox`( lck_uid , lck_ref , lck_data ) VALUES ( "'.$uid.'" , "'.md5( $uniqid.$password ).'" , "'.cleanString( $password , array( '=' , '+' , '/' ) ).'" );';
		$query = mysql_query( $query , $connectionId ) or exit( 'savePassword_02' );

		$dbHash = mysql_fetch_array( mysql_query( 'SELECT GROUP_CONCAT( lck_data SEPARATOR \';\' ) dbHash FROM lck_strongbox WHERE lck_uid = "'.$uid.'"' ));

		echo json_encode( array( 'msg' => 'ok' , 'databaseHash' => md5($dbHash['dbHash']) , 'uniqid' => md5( $uniqid.$password ) ) );

	}









	function confirmDevice()
	{


		$raw = getRaw( $_POST['d'] , 'lck_uid' , md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ) , 'confirmDevice' , 'lck_public' );

		$decrypted = $raw[0];
		$publ = $raw[1]['lck_public'];


		if( mysql_num_rows(mysql_query('SELECT * FROM lck_credentials WHERE lck_uid = "'.$decrypted['u'].'"')) == 1 ){

			session_regenerate_id( true );
			$new_session = session_id();

			mysql_query( 'UPDATE lck_devices SET lck_uid = "'.$decrypted['c'].'" , lck_sessid = "'.$new_session.'" WHERE lck_sessid = "'.$decrypted['s'].'"' );
			echo sliceString( json_encode( array( 'msg' => 'complete' , 'sessid' => $new_session ) ) , $publ );

		}else
			echo 'confirmDevice_02';

	}



	//-------------------------------------------------------------------------------------





	function newUser()
	{

		global $connectionId;

		$query = 'SELECT `lck_uid` FROM lck_credentials WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'"';

		$query = mysql_query( $query , $connectionId );

		if( $query ){
			
			if( mysql_num_rows( $query ) >= 1 ){
				
				$query = 'SELECT lck_public FROM lck_devices WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'"';
				$query = mysql_fetch_array( mysql_query( $query , $connectionId ) );
				exit( $query['lck_public'] );

			}

		}else
			exit('newUser_02');


		// Generamos un par de claves para el servidor
		$rsa = new Crypt_RSA();
		$rsa->setPrivateKeyFormat( CRYPT_RSA_PRIVATE_FORMAT_PKCS1 );
		$rsa->setPublicKeyFormat(  CRYPT_RSA_PUBLIC_FORMAT_PKCS1  );
		extract( $rsa->createKey( 512 ) );

		$privatekey = str_replace( "\n" , ' ' , $privatekey );
		$privatekey = str_replace( "\r" , '' , $privatekey );
		$privatekey = trim( $privatekey );

		$publickey = str_replace( "\n" , ' ' , $publickey );
		$publickey = str_replace( "\r" , '' , $publickey );
		$publickey = trim( $publickey );

		$query = 'INSERT INTO lck_credentials( lck_uid , lck_credentials )';
		$query.= 'VALUES( "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'" , "{}" );'."\n";
		
		$query2 = 'INSERT INTO lck_devices( lck_uid , lck_dev , lck_public , lck_private , lck_active )';
		$query2.= 'VALUES( "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'" , "{}" , "'.$publickey.'" , "'.$privatekey.'" , "0" )';


		if( mysql_query( $query , $connectionId ) ){

			if( mysql_query( $query2 , $connectionId ))
				echo $publickey;
			else
				exit('e3');

		}

		
		//mysql_close( $con );

	}




	//----------------------------------------------------------------------------
	// WORKING FUNCTIONS WITH NEW CHANGES


	function verifyAccount(){



		$raw = getRaw( $_POST['d'] , 'lck_uid' , md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ) , 'verifyAccount' , 'lck_public'  );

		$publ = $raw[1]['lck_public'];
		$data = $raw[0];

		session_regenerate_id( true );
		$new_session = session_id();

		if( mysql_query('UPDATE lck_devices SET lck_active = "1" , lck_sessid = "'.$new_session.'", lck_name = "'.$data['device'].'", lck_uid = "'.$data['r'].'" WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'"'))
			echo sliceString( json_encode( array( 'msg' => 'ok' , 'sessid' => $new_session ) ) , $publ );
		else
			echo 'error';

	}



	function newUser2()
	{

		$credentials = getRaw( $_POST['d'] , 'lck_uid' , md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ) ,'newUser2' );
		session_regenerate_id( true );
    	$sessionid = session_id();
    	global $connectionId;

    	$query = 'UPDATE lck_credentials SET lck_uid = "'.$credentials['u'].'", lck_credentials = "'.$credentials['c'].'" WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'";';
    	$query2= 'UPDATE lck_devices SET lck_name = "Inicial" , lck_dev = "'.$credentials['dev'].'", lck_sessid = "'.$sessionid.'", lck_public = "'.$credentials['p'].'" , lck_uid = "'.$credentials['k'].'", lck_active = "1" WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'"';

		mysql_query( $query  , $connectionId ) or exit( 'newUser2_02' );
		mysql_query( $query2 , $connectionId ) or exit( 'newUser2_03' );

		exit( sliceString(  json_encode( array( 'msg' => 'complete' , 'sessid' => $sessionid ) ) , $credentials['p'] ) );

	}


	function checkLogin()
	{

		global $connectionId;
   		$decrypted = getRaw( $_POST['d'] , 'lck_uid' , md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ) );

		$query = 'SELECT * FROM lck_credentials WHERE lck_uid = "'.$decrypted['u'].'"';
		$query = mysql_query( $query , $connectionId );

		if( mysql_num_rows( $query ) == 0 )
			exit('e12');

		if( !$query )
			exit('e11');
		else{

			$account = mysql_fetch_array( $query );
			$credentials = $account['lck_credentials'];
			$account = $account['lck_uid'];
			$rand = 'login_'.rand( 1000 , 9999 );

			if( mysql_num_rows( mysql_query( 'SELECT lck_uid FROM lck_devices WHERE lck_dev = "'.$decrypted['dev'].'" and lck_active = "1"' , $connectionId )) == 0 ){ 	

				if( mysql_query( 'UPDATE lck_devices SET lck_sessid = "'.$rand.'" , lck_dev = "'.$decrypted['dev'].'", lck_public = "'.$decrypted['q'].'" WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'"' , $connectionId )){

					echo sliceString(  json_encode( array( 'msg' => 'wait' , 'sessid' => $rand , 'account' => $account )) , $decrypted['q'] );

				}else{
					echo 'e13';
				}

			}else{

				mysql_query( 'UPDATE lck_devices SET lck_sessid = "'.$rand.'", lck_public = "'.$decrypted['q'].'" WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'"' , $connectionId );
				echo sliceString(  json_encode( array( 'msg' => 'confirm' , 'sessid' => $rand , 'credentials' => $credentials )) , $decrypted['q'] );

			}

		}

	}


	function updateCode(){

		$data = getRaw( $_POST['d'] , 'lck_sessid' , $_POST['sessid'] , 'updateCode' );
		$account = $data['a'];

		$randomCode = rand( 1000 , 9999 );
		$until = time()+( 60*30 );

		$check = json_encode( array( 'until' => date( 'Y-m-d H:i:00' , $until ) , 'code' => $randomCode ) );
		global $connectionId;

		if( mysql_query( 'UPDATE lck_credentials SET lck_check = "'.base64_encode( $check ).'" WHERE lck_uid = "'.$data['a'].'"' , $connectionId ) )
			echo json_encode( array( 'until' => date( 'd/m/Y - H:i:00' , $until ) , 'code' => $randomCode ) );
		else
			exit('updateCode_02');

		exit();

	}

	function checkCode(){

		global $connectionId;
		$data = getRaw( $_POST['d'] , 'lck_uid' , md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ) , 'checkLogin' );

		$query = mysql_query( 'SELECT * FROM lck_devices WHERE lck_uid = "'.md5( $_SERVER['REMOTE_ADDR'].$_POST['tmpSession'] ).'" AND lck_sessid = "'.$data['c'].'"' , $connectionId );

		if( mysql_num_rows( $query ) == 1 ){
	
			$userPublic = mysql_fetch_array( $query );
			$userPublic = $userPublic['lck_public']; 

			$query = mysql_query( 'SELECT lck_credentials,lck_check FROM lck_credentials WHERE lck_uid = "'.$data['a'].'"' , $connectionId );
			$query = mysql_fetch_array( $query );

			$checkCode = json_decode( base64_decode( $query['lck_check'] ) , 1 );

			if( strtotime( $checkCode['until'] ) > time() ){

				if( $checkCode['code'] == $data['v'] ){
					
					session_regenerate_id( true );
					$new_session = session_id();
					echo sliceString(  json_encode( array( 'msg' => 'ok' , 'credentials' => $query['lck_credentials'] , 'session' => $new_session )) , $userPublic );
					
					mysql_query( 'UPDATE lck_credentials SET lck_check = "wait" WHERE lck_uid = "'.$data['a'].'"' , $connectionId );
				
				}else
					echo 'checkCode_03';
			}else{
				echo 'checkCode_02'; // Sesión caducado
			}

		}else{
			echo 'checkCode_04';
		}

	}



?>