
/*! LockedShield
* Licensed under the GPLv3 License - <http://www.gnu.org/licenses/gpl-3.0.txt>
* Author: Moisés Lodeiro Santiago
* Version: 0.2
* Requires: jQuery/Zepto
*/


$( document ).ready(
  
    


  function()
  {



        
        /* Variables:
        * plainPassword: Variable temporal. No se guarda en el cliente
        * selectedServer: Variable que indica la ruta al servidor a conectar
        */

        var isPhoneGap = 0; // Enable/Change to "true" or "1" if program runs with PhoneGap
        var plainPassWord = ''; // Plaintext Password
        var selectedServer = ''; // Selected server ( full url )
        var serverList = []; // Server list [ url , port , name ]
        var passwordList = []; // Password list [ reference , md5 , [ decryptedPassword ] ]
        
        var tmp  = ''; // Temporal variable to use in some methods
        var tmp2 = ''; // Temporal variable to use in some methods


        /*
        */


        function visiblePanel( e )
        {

            if( $(e).hasClass("ui-panel-open") == true )
               return true;
            else
               return false;
            
        }


        if( isPhoneGap ){
            $("div [data-role='header']").hide();
            $("div [role='main']").css({ 'padding-left' : '5px' , 'padding-right' : '5px' });
        }


        $( "body" ).on( "swiperight", function(){ // deslizas HASTA la derecha


            if( isPhoneGap ){
                if( $.mobile.activePage.attr('id') == "loginForm" ){
                    $("body").pagecontainer( "change", "#mainlocked" );
                }else if( $.mobile.activePage.attr('id') == "mainlocked" ){
                    $( "#leftMenu" ).panel( "open" );
                }
            }
            
            

        });



        $( "body" ).on( "swipeleft", function(){ // deslizas HASTA la izquierda

            if( isPhoneGap ){

                if( $.mobile.activePage.attr('id') == "mainlocked" && visiblePanel("#leftMenu") )
                    $( "#leftMenu" ).panel( "close" );
                else
                    $("body").pagecontainer( "change", "#loginForm" );

            }

        });

        $("div [role='main']").css('margin-bottom','50px');


        /* La función sliceString( text ) recoge un parámetro de texto
         * y la trocea en cadenas de 50 caracteres ( limitación dada por 
         * el cifrado en 512 de la librería de RSA ). Luego cifra estas
         * por separado y las junta en un objeto json.
         */

        function sliceString( text ){


            if( localStorage.serverPublicKey == 'undefined' )
            {
                getPubKey();
            }


            var numSlices = Math.ceil( text.length/50 );
            
            if( numSlices <= 0 )
                return false;

            var slices = [];

            var encrypt = new JSEncrypt({ default_key_size : 512 });
            encrypt.setPublicKey( localStorage.serverPublicKey );

            for( i = 0 ; i < numSlices ; i++ ){
                slices[i] = encrypt.encrypt( text.substr( i*50 , 50  ) );
            }

            return slices.toString();

        }


        function getRandomPassword( length ){

            ret = '';

            for( i = 0 ; i < length ; i++ ){

                do {
                    next = Math.floor( Math.random()*(125-33+1)+33 );
                } while( next == 60 || next == 62 );

                ret += String.fromCharCode( next );
            }

            return ret;

        }

        /* La función joinString hace lo contrario que la sliceString,
         * es decir, obtiene un objeto json y va uniendo las partes para
         * obtener la cadena resultante previamente troceada.
         */

        function joinString( text ){

            var json = JSON.parse( text );
            var decrypt = new JSEncrypt();

            decrypt.setPrivateKey( localStorage.privKey );
            text = '';
            
            for( f in json )
                text += decrypt.decrypt( json[f] );
                
            return text;

        }

        /* En los campos de texto se limitan los caracteres
         * a los normales en un mapa ascii para evitar que
         * las cadenas cifradas por RSA superen el límite y
         * de un error.
         */

        $('input:text').keypress(function (e) {

            if( e.charCode >= 32 && e.charCode <= 126 )
                return true;

            e.preventDefault();

            info("Por motivos de seguridad sólo se admiten letras, números y símbolos comunes.");
            return false;

        });


        /* La función warningModal recibe un texto y llama a
         * la librería "noty" para mostrar por pantalla un aviso modal
         */

        function warningModal( utext ){

            var n = noty({
                text        : utext ,
                type        : 'warning',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                modal: true
            });
        }

        /* La función warning recibe un texto y llama a
         * la librería "noty" para mostrar por pantalla un aviso
         */

        function warning( utext ){

            var n = noty({
                text        : utext ,
                type        : 'warning',
                dismissQueue: true,
                layout      : 'top',
                theme       : 'defaultTheme',
                timeout: 5000,
                maxVisible: 2
            });
        }

        /* La función info recibe un texto y llama a
         * la librería "noty" para mostrar por pantalla un aviso
         */

        function info( utext ){

            var n = noty({
                text        : utext ,
                type        : 'information',
                dismissQueue: true,
                layout      : 'top',
                theme       : 'defaultTheme',
                timeout: 5000,
                maxVisible: 2
            });
        }

        /* La función infogModal recibe un texto y llama a
         * la librería "noty" para mostrar por pantalla un aviso modal
         */

        function infoModal( utext , time ){

            var n = noty({
                text        : utext ,
                type        : 'information',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                timeout: time,
                maxVisible: 2,
                modal: true
            });
        }

    /* La función error recibe un texto y llama a
    *  la librería "noty" para mostrar por pantalla un aviso
    */

    function error( utext ){
    
            var n = noty({
            text        : utext ,
            type        : 'error',
            dismissQueue: true,
            layout      : 'top',
            theme       : 'defaultTheme',
            timeout: 5000,
            maxVisible: 2
        });
    }

    
    $("#server-select").change(function(e){

        if( $(this).val() == 'local' )
        {

            $("#user").parent().hide();
            Apprise( '<b>Información:</b><br /><p>En modo "Dispositivo" todos los datos quedan registrados en el terminal y no en un servidor externo.</p>' );
        
        }
        else
        {

            $("#user").parent().show();
            $("#registerButton").show();
        
        }
    
        selectedServer = $(this).val();
        localStorage.setItem('selectedServer',selectedServer);

    });


    $('#password1').hideShowPassword({
        show: false,
        innerToggle: true,
        toggleText: '',
        touchSupport: Modernizr.touch
    });


    $('#generate').click(function() {
        $('#password1').val( getRandomPassword( 6 ) );
        $('#password1').showPassword();
    });


    function isJSON( str )
    {
    
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    
    }


    $('body').on( 'click', '.decryptPassword', function() {
        aux = Aes.Ctr.decrypt( $(this).val() , plainPassWord , 256 );
        $(this).parent().html('<button value="'+$(this).val()+'" class="decryptPasswordRemove ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-forbidden">'+aux+'</button>');
    });

    $('body').on( 'click', '.decryptPasswordRemove', function() {
        $(this).parent().html('<button value="'+$(this).val()+'" class="decryptPassword ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-eye">Mostrar</button>');
    });

    $('body').on( 'click', '.deleteServer', function() {
       
        
        serverAux = JSON.parse( localStorage.serverList );
        
        if( serverAux[$(this).val()][0] == localStorage.selectedServer )
            localStorage.removeItem('selectedServer');

        serverAux.splice( $(this).val() , 1 );
        
        if( serverAux.length == 0 ){
            localStorage.removeItem('serverList');
        }else{
            serverAux = JSON.stringify( serverAux );
            localStorage.setItem( 'serverList' , serverAux );
        }
        
        $(this).parent().parent().parent().fadeOut();


    });

    $("#synchronizePasswords").click(function(){

        $("#passwordsLists").html('');

        check = { 
            session : md5( localStorage.session ),
            locpubl : md5( localStorage.pubKey  )
        };

        $.ajax({

            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "getPasswords",
                data  : sliceString( JSON.stringify( check )),
                sessid: localStorage.session
            },
            dataType: 'text',
            success: function( data )
            {


                if( isJSON( data )){

                    data = joinString( data );

                    localStorage.setItem( 'dbHash' , md5( data ) );

                    passwordAux = data.split( ";" );
                    aux = '';

                    $("#passwordMessageBoxPanel").remove();

                    for( i = 0 ; i < passwordAux.length ; i++ ){
                        aux = JSON.parse( Aes.Ctr.decrypt( passwordAux[i] , plainPassWord , 256 ));
                        $("#passwordsLists").append('<li class="list-group-item"><div class="ui-grid-a ui-responsive"><div class="ui-block-a"><div class="ui-body ui-body-d">'+aux.r+'</div></div><div class="ui-block-b"><div class="ui-body ui-body-d"><button value="'+aux.p+'" class="decryptPassword ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-eye">Mostrar</button></div></div></div></li>');
                    }


                }else{
                    
                    switch( data )
                    {
                        case "" : 
                            $("#passwordMessageBox").html("Aún no ha guardado ninguna contraseña");
                            break;
                        case 'cleanPasswords': 
                            localStorage.removeItem('dbHash');
                            break;
                    }

                }

                   
            },
            error: function(result) {
                error("No se ha podido procesar el registro. ¿Es correcto el servidor?");
                $.unblockUI();
            }

        });

    });




    $("#savePassword").click( function(){



        aesPassword = Aes.Ctr.encrypt( $.trim( $("#saveNewPassword").val() ) , plainPassWord , 256 );
        var passwordData = {
            p: aesPassword,
            r: $.trim( $("#appName").val() )
        };

        encryptedData = Aes.Ctr.encrypt( JSON.stringify( passwordData ) , plainPassWord , 256 );
        encryptedData = sliceString( encryptedData );

        $.ajax({

                url: selectedServer,
                global: true,
                async: true,
                type: "POST",
                data: 
                { 
                    action: "savePassword",
                    data: encryptedData,
                    sessid: localStorage.session,
                    dbHash: (( localStorage.dbHash != undefined )?( localStorage.dbHash ):( '' ))
                },
                dataType: 'text',
                success: function( data )
                {

                    if( isJSON( data )){


                        data = JSON.parse( data );
                        if( data.msg == 'ok' ){


                            $("#appName").val("");
                            $("#saveNewPassword").val("");

                            localStorage.setItem( "dbHash" , data.databaseHash );
                            passwordAux = [ $.trim($("#appName").val()) , data.uniqid , aesPassword ];
                            passwordList.push( passwordAux );
                            jPasswordList = JSON.stringify( passwordList );
                            localStorage.setItem( 'passwordList' , Aes.Ctr.encrypt( jPasswordList , plainPassWord , 256 ) );
                            info( "Contraseña guardada correctamente" );

                            $("#passwordsLists").append('<li class="list-group-item"><div class="ui-grid-a ui-responsive"><div class="ui-block-a"><div class="ui-body ui-body-d">'+$.trim($("#appName").val())+'</div></div><div class="ui-block-b"><div class="ui-body ui-body-d"><button value="'+aesPassword+'" class="decryptPassword ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-eye">Mostrar</button></div></div></div></li>');


                        }else{

                            console.log("Error desconocido");
                        
                        }

                    }else{

                        if( data == "syncIt" )
                            warning("Antes de guardar una contraseña debes sincronizar con el servidor");

                        console.log("Error?"+data);

                    }

                   
                },
                error: function(result) {
                    error("No se ha podido procesar el registro. ¿Es correcto el servidor?");
                    $.unblockUI();
                }

            });






    });


    $("#registerButton").click(function(){


        if( selectedServer == '' )
        {
        
            warning('Debe primero seleccionar un servidor');
            $.mobile.changePage("#serverManager_add");
            return false;
        
        }


        if( $.trim( $("#user").val() ).length <= 5 ){
            warning('El usuario debe tener, al menos, 6 caracteres.');
            return false;
        }


        if( $.trim( $("#password1").val() ).length <= 5 ){
            warning('La cotraseña debe tener, al menos, 6 caracteres.');
            return false;
        }

        if( localStorage.serverPublicKey == undefined || localStorage.serverPublicKey == 'false' || localStorage.serverPublicKey == '' ){
            getPubKey();
        }

        if( $.trim( $("#user").val() ) == '' || $.trim( $("#password1").val() ) == '' )
        {
            warning("Usuario y contraseña necesarios");
            return false;

        }
        else
        {

            //if( localStorage.serverPublicKey == undefined )
            //    getPubKey();

            if( localStorage.serverPublicKey == undefined )
                return false;

            rand_hash = getRandomString(5);
            var cred = Aes.Ctr.encrypt( JSON.stringify({
                ruid: md5( $.trim( localStorage.privKey )+$.trim($("#user").val())+rand_hash )
            }) , $.trim( $("#password1").val() ) , 256 );


            login = md5( $.trim( $.trim($("#user").val())+$.trim($("#password1").val()) ) );

            localStorage.setItem( 'account' , login );

            send = {
                p: localStorage.pubKey,
                u: login,
                c: cred,
                k: md5( $.trim( localStorage.privKey )+$.trim($("#user").val())+rand_hash ),
                dev: md5( localStorage.deviceId+$.trim($("#user").val())+navigator.userAgent )
            };

            send = JSON.stringify( send );
            send = sliceString( send );

            var aux_data;

            $.ajax({

                url: selectedServer,
                global: true,
                async: true,
                type: "POST",
                data: 
                { 
                    action: "registerUid",
                    d: send,
                    tmpSession: localStorage.tmpSession
                },

                dataType: 'text',
                success: function( data )
                {



                    if( isJSON( data )){
                        data = JSON.parse(joinString( data ));

                        if( data.msg == "complete" ){

                            localStorage.setItem( 'session' , data.sessid );
                            localStorage.setItem( 'id' , md5( $.trim( localStorage.privKey )+$.trim($("#user").val()) ) );
                            localStorage.setItem( 'name' , $.trim( $("#user").val() ) );
                            localStorage.setItem( 'logged' , true );
                            
                            localStorage.setItem( 'password' , JSON.stringify({ p : $.trim($("#password1").val()) }) );
                            plainPassWord = $.trim($("#password1").val());

                            document.location.href = 'logged.html';

                        }else{
                            alert(data);
                        }
                    }else{

                        switch( data ){
                            case 'newUser_02'  : erro("yyy"); break;
                            case 'newUser2_01' : error('xxx'); break;
                            case 'newUser2_02' : error('Ese usuario ya existe'); break;
                            case 'newUser2_03' : error('Error actualizando datos.'); break;
                            case 'e4' : warning('Ese usuario ya se encuentra registrado en el sistema'); break;
                        }
                        console.log(data);
                    }
                },
                error: function(result) {
                    error("No se ha podido procesar el registro. ¿Es correcto el servidor?");
                    $.unblockUI();
                }

            });

        }



    });


    /* TODO
    *
    */

    function getPass( token ){
        alert( token );
    }


    /*
     * Cuando se hace click en "reloadsb" ( strong box )
     * se manda una petición al servidor que recarga los
     * datos en la página.
     */


    $("#realoadsb").click( function()
    { 
        loadStrongBox(); 
    });



    function sumMinutes( minutes ){
        d = new Date();
        return ((d.getHours()+(Math.floor((d.getMinutes()+minutes)/60)))%24)+':'+((d.getMinutes()+minutes)%60);
    }


    function updateCode(){

        $("#generatedCode").html( "Generando...");

        $.ajax({

            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "updateCode",
                d: sliceString( JSON.stringify({ a : localStorage.account })),
                sessid: localStorage.session
            },
            dataType: 'text',
            success: function( data )
            {
                
                if( isJSON( data )){
                    data = JSON.parse( data );
                    $("#generatedCode").html( "<b class='ui-bar ui-bar-a ui-corner-all' style='text-align:center; font-size: 20px'>"+data['code']+"</b>Válido hasta: "+data['until']+"<hr>" );
                }else{
                    console.log( data );
                }

            } // success
        });

    }


    $("#genCode").click(function(){

        updateCode();

    });



    $('body').on( 'change', '.onoffDevice', function() {

        //console.log( $(this).attr('name') );
        check = { 
            session : md5( localStorage.session ),
            locpubl : md5( localStorage.pubKey  ),
            device  : $(this).attr('name'),
            status  : $(this).val()
        };

        $.ajax({

            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "updateDevices",
                data  : sliceString( JSON.stringify( check )),
                sessid: localStorage.session
            },            
            dataType: 'text',
            success: function( data )
            {

                if( data == 'ok' )
                    info('Dispositivos actualizados');

               //console.log(data);

            } // success
        });



    });


    $("#syncDevices").click(function(){

        $("#allowedDevicesUl").html("Cargando...");

        check = { 
            session : md5( localStorage.session ),
            locpubl : md5( localStorage.pubKey  )
        };

        $.ajax({

            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "syncDevices",
                data  : sliceString( JSON.stringify( check )),
                sessid: localStorage.session
            },            
            dataType: 'text',
            success: function( data )
            {

                if( isJSON( data )){

                    data = JSON.parse( joinString( data ));
                    $("#allowedDevicesUl").html("");

                    $.each( data , function( index , value ) {

                        value = JSON.parse( value );
console.log( value );

                        addData = '<li class="list-group-item"><div class="ui-grid-a ui-responsive"><div class="ui-block-a">';
                        addData+= '<div class="ui-body ui-body-d">'+(( value.name_ != '')?( (( value.name_ == "Inicial" )?( "Dispositivo de registro" ):( Aes.Ctr.decrypt( value.name_ , plainPassWord , 256 )  ))  ):( 'Sin nombre' ))+'</div></div><div class="ui-block-b">';
                        addData+= '<div class="ui-body ui-body-d" style="text-align:right;">';
                        addData+= '<select id="slider-flip-m" name="'+index+'" class="onoffDevice" data-role="slider" data-mini="true">';
                        addData+= '<option value="off" '+(( value.active == "0" )?("selected"):(""))+'>No</option><option value="on" '+(( value.active == "1" )?("selected"):(""))+'>Sí</option></select></div>';
                        addData+= '</div></div></li>';

                        $("#allowedDevicesUl").append( addData ).trigger("create");

                    }); 

                }else
                    console.log( data );


            } // success
        });




    });



    $("#checkCode").click(function(){


        $.ajax({

            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "checkCode",
                d: sliceString( JSON.stringify({ a : tmp2 , c : tmp , v : $.trim($("#checkCodeNum").val()) })),
                tmpSession: localStorage.tmpSession
            },
            dataType: 'text',
            success: function( data )
            {
                

                if( isJSON( data )){

                    data = JSON.parse( joinString( data ));

                    if( data['msg'] == 'ok' ){

                        uid = JSON.parse( Aes.Ctr.decrypt( data['credentials'] , $.trim($("#password").val()) , 256 ));
                        uid = uid['ruid'];

                        $.ajax({

                            url: selectedServer,
                            global: true,
                            async: true,
                            type: "POST",
                            data: 
                            { 
                                action: "verifyAccount",
                                d: sliceString( JSON.stringify({ r: uid , device : Aes.Ctr.encrypt( $.trim($("#checkCodeNumDev").val()) , $.trim($("#password").val()) , 256 ) })),
                                tmpSession: localStorage.tmpSession
                            },
                            dataType: 'text',
                            success: function( data )
                            {
                                
                                if( isJSON( data )){

                                    console.log(data);

                                    data = JSON.parse( joinString( data ));
                                    if( data['msg'] == 'ok' ){

                                        localStorage.setItem( 'session' , data['sessid'] );
                                        localStorage.setItem( 'account' , tmp2 );
                                        localStorage.setItem( 'id' , uid );
                                        localStorage.setItem( 'name' , $.trim( $("#name").val() ) );
                                        localStorage.setItem( 'logged' , true );
                                        localStorage.setItem( 'password' , Aes.Ctr.encrypt( $.trim($("#password").val()) , $.trim($("#password").val()) , 256 ) );
                                        plainPassWord = $.trim($("#password").val());
                                        document.location.href = 'logged.html';

                                    }else{
                                        error("Error desconocido");
                                    }

                                }else{
                                    warning("Error"+data);
                                }


                            } // success

                        });



                    }else{
                        error("Error desconocido.");
                        return false;
                    }

                }else{


                    switch( data ){
                        case 'checkCode_01' : error('No se encontró la sesión.'); break;
                        case 'checkCode_02' : warning('La sesión se ha caducado'); break;
                        case 'checkCode_03' : warning('El código es inválido.'); break;
                    }

                    console.log(data);

                }

            } // success
        });





    });


    /*
     * Cuando se hace click en el botón "login" ( panel derecho )
     * se llama automáticamente la función que recoge los datos
     * para realizar el login.
     */

    $( "#login" ).click( function()
    {

        
        if( selectedServer == '' ){
            warning('Debe primero seleccionar un servidor');
            $.mobile.changePage("#serverManager_add");
            return false;
        }

        if( localStorage.serverPublicKey == undefined || localStorage.serverPublicKey == 'false' || localStorage.serverPublicKey == '' ){
            getPubKey();
        }

        if( localStorage.tmpSession == undefined ){
            localStorage.setItem( 'tmpSession' ,  getRandomString(40) );
            alert("aparezco");
        }

        var login = {
            u : md5( $.trim( $.trim($("#name").val())+$.trim($("#password").val()) ) ),
            q : localStorage.pubKey,
            dev : md5( localStorage.deviceId+$.trim($("#name").val())+navigator.userAgent )
        };

        send = JSON.stringify( login );
        send = sliceString( send );

        $.ajax({
            
            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "checkLogin",
                d: send,
                tmpSession: localStorage.tmpSession
            },
            
            dataType: 'text',
            success: function( data )
            {


                if( isJSON( data )){
                  
                    data = JSON.parse( joinString( data ));

                    if( data['msg'] == 'wait' ){

                        $.mobile.changePage("#loginWait");
                        tmp  = data['sessid'];
                        tmp2 = data['account']; 

                    }else{

                        if( data['msg'] == 'confirm' ){

                            credentials = JSON.parse( Aes.Ctr.decrypt( data['credentials'] , $.trim($("#password").val()) , 256 ));
                            credentials = credentials['ruid'];

                            $.ajax({

                                url: selectedServer,
                                global: true,
                                async: true,
                                type: "POST",
                                data: 
                                { 
                                    action: "confirmDevice",
                                    d: sliceString( 
                                        JSON.stringify({ 
                                                c : credentials , 
                                                s : data['sessid'],
                                                u : md5( $.trim( $.trim($("#name").val())+$.trim($("#password").val()) ) )
                                            })
                                        ),
                                    sessid: localStorage.session,
                                    tmpSession: localStorage.tmpSession
                                },
                                dataType: 'text',
                                success: function( data )
                                {

                                    if( isJSON( data )){

                                        data = JSON.parse(joinString( data ));
                                        if( data['msg'] == 'complete' ){

                                            if( localStorage.account == undefined )
                                                localStorage.setItem( 'account' , md5( $.trim( $.trim($("#name").val())+$.trim($("#password").val()) ) ));

                                            localStorage.setItem( 'session' , data['sessid'] );
                                            localStorage.setItem( 'name' , $.trim( $("#user").val() ) );
                                            localStorage.setItem( 'logged' , true );
                                            localStorage.setItem( 'password' , JSON.stringify({ p : $.trim($("#password").val()) }) );
                                            document.location.href = 'logged.html';

                                        }

                                    }else
                                        console.log(data);
                                    
                                } // success

                            });



                        }

                    }

                }else{


                    $("#notifications").html(data);

                    if( data == 'e12' ){
                        warning("Usuario o contraseña erróneo");
                        return false;
                    }else
                        console.log( data );

                }

                

       

            },
            error: function(result) {
                error("No se ha podido procesar el login. ¿Es correcto el servidor?");
                $.unblockUI();
            } // success
        }); // ajax
    }); // login






    $("#serverAdd").click(function(){


        if( $.trim($("#serverName").val()) == '' || $.trim($("#serverUrl").val()) == '' )
            warning("Debe rellenar el nombre/alias y la dirección http del servidor.");
        else
        {

            port = 80;
            serverAux = [ $.trim($("#serverUrl").val()) , $.trim(port) , $.trim($("#serverName").val()) ];
            serverList.push( serverAux );

            localStorage.setItem( 'serverList' , JSON.stringify( serverList ) );

            $("#server-select").append( '<option '+(( serverList.length == 1 )?('selected'):'')+' value="'+$.trim( $("#serverUrl").val() )+'" >'+$.trim( $("#serverName").val() )+'</option>' );
           
            if( serverList.length == 1 ){

                $("#server-select").selectmenu( "refresh" );
                selectedServer = $.trim( $("#serverUrl").val() );
                localStorage.setItem('selectedServer',selectedServer);

            }

            /*element = '<div data-role="collapsible" data-mini="true" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">';
            element+= '<h3>'+$.trim($("#serverName").val())+'</h3>';
            element+= '<ul data-role="listview" data-inset="false" >';
            element+= '<li>Dirección: '+$.trim( $("#serverUrl").val() )+'</li>';
            element+= '<li style="list-style-type: none; padding-top:10px;">';
            element+= '<div class="ui-grid-a"><div class="ui-block-a"><a href="#" class="ui-shadow ui-btn ui-corner-all ui-mini" style="">Editar</a></div>';
            element+= '<div class="ui-block-b"><a href="#" class="ui-shadow ui-btn ui-corner-all ui-mini" style="background-color: #E37366; color: white;">Borrar</a></div>';
            element+= '</div></li>';
            element+= '</ul></div>';*/

            num = JSON.parse( localStorage.serverList ).length;


            element = '<li class="list-group-item">';
            element += '<div class="row"><div class="col-md-8">'+$.trim($("#serverName").val())+'<br />'+$.trim( $("#serverUrl").val() )+'</div><div class="col-md-2">';
            element += '<button class="deleteServer ui-btn ui-icon-delete ui-btn-inline ui-btn-icon-notext" value="'+num+'">Borrar</a>';
            element += '</div><div class="col-md-2" style="text-align:center;"><input type="radio" name="selectedServerRadio" '+(( serverList.length == 1 )?('checked'):'')+' /></div></div></li>';

            $("#showServerList").append( element );
            
           // $(".bigRadio").controlgroup("refresh");



            //$('.ui-radio').buttonset("refresh");
            //$('div').find('div[data-role=collapsible]').collapsible();
            $.mobile.changePage("#serverManager");

        }


    });






function RESET(){

    localStorage.removeItem('deviceId'); // NO SE DEBE BORRAR
    localStorage.removeItem('privKey');
    localStorage.removeItem('pubKey');
    localStorage.removeItem('serverPublicKey');
    localStorage.removeItem('uid');
    localStorage.removeItem('logged');
    localStorage.removeItem('name');
    localStorage.removeItem('password');
    localStorage.removeItem('ruid');
    localStorage.removeItem('account');
    localStorage.removeItem('device');
    localStorage.removeItem("session");
    localStorage.removeItem('dbHash');
    localStorage.removeItem('tmpSession');
    localStorage.removeItem('serverList');
    localStorage.removeItem('passwordList');
    localStorage.removeItem('selectedServer');
    document.location.href = "register.html";

}



$("#RESET").click(function(){
    RESET();
});


    function getRandomString( size ){

        chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        result = '';

        for( i = 0 ; i < size ; i++ )
            result += chars[ Math.ceil( Math.random() * chars.length ) ];

        return result;

    }


    $("#pubkey").click(function(){
        getPubKey();
    });


    function  getPubKey()
    {

        var result = "";

        if( selectedServer == '' ){
            return false;
            console.log("No hay servidor");
        }

        $.ajax({

            url: selectedServer,
            async: false,
            type: "POST",
            data: 
            { 
                action: "sayHi",
                tmpSession: localStorage.tmpSession
            },
            dataType: 'text',
            success: function( data )
            {

                if( data == 'newUser_02' ){
                    error("Error obteniendo clave pública del servidor.");
                    console.log(data);
                    return false;
                }

                localStorage.setItem( "serverPublicKey" , data );

            },
            error: function( data ){
                alert("Error obteniendo clave pública del servidor");
            }
        });


    }


    function getStatus(){

        check = { 
            session : md5( localStorage.session ),
            locpubl : md5( localStorage.pubKey  )
        };

        $.ajax({

            url: selectedServer,
            global: true,
            async: true,
            type: "POST",
            data: 
            { 
                action: "getStatus",
                data  : sliceString( JSON.stringify( check )),
                sessid: localStorage.session
            },            
            dataType: 'text',
            success: function( data )
            {

                if( data == 'logout' )
                    $("#logout").trigger('click');
                else
                    console.log(data);

            } // success
        });   
    
    }


    if( localStorage.privKey == undefined ){

        localStorage.setItem( 'tmpSession' ,  getRandomString( 40 ) );

        if( localStorage.deviceId == undefined )
            localStorage.setItem( 'deviceId' , getRandomString( 10 ) ); // NO BORRAR


        var keySize = 512;
        crypt = new JSEncrypt({ default_key_size : keySize });
        crypt.getKey();

        localStorage.setItem( 'privKey', $.trim( crypt.getPrivateKey() ));
        localStorage.setItem( 'pubKey' , $.trim( crypt.getPublicKey() ));

        localStorage.setItem( 'logged' , false );
        $.noty.closeAll();

    }



    if( localStorage.serverList != undefined && localStorage.logged == 'false' ){
    
        serverList = JSON.parse( localStorage.serverList );
        element = '';

        for( i = 0 ; i < serverList.length ; i++ ){
            
            $("#server-select").append( '<option '+((i==0)?('selected'):(''))+' value="'+serverList[i][0].toString()+'">'+serverList[i][2].toString()+'</option>' );


            element += '<li class="list-group-item">';
            element += '<div class="row"><div class="col-md-8">'+serverList[i][2].toString()+'</div><div class="col-md-2">';
            element += '<button class="deleteServer ui-btn ui-icon-delete ui-btn-inline ui-btn-icon-notext" value="'+i+'">Borrar</a>';
            //element += '<a href="#" class="ui-btn ui-icon-edit ui-btn-inline ui-btn-icon-notext">Icon only</a>';
            element += '</div><div class="col-md-2" style="text-align:center;"><input '+(( localStorage.selectedServer == serverList[i][0] )?("checked"):(""))+' type="radio" name="selectedServerRadio" /></div></div></li>';

        }

        selectedServer = serverList[0][0].toString();
        localStorage.setItem('selectedServer',selectedServer);

        $("#showServerList").append( element );
        $('div').find('div[data-role=collapsible]').collapsible();
        $("#server-select").selectmenu( "refresh" );

    }


    if( selectedServer == '' && localStorage.selectedServer != undefined )
        selectedServer = localStorage.selectedServer;


    if( isJSON( localStorage.password )){

        aux = JSON.parse( localStorage.password );
        plainPassWord = aux.p;
        localStorage.setItem( 'password' , Aes.Ctr.encrypt( plainPassWord , plainPassWord , 256 ));

    }


    if( localStorage.logged == "true" && localStorage.question == undefined ){

        //$("#userMessage").html('<p class="bg-danger dangerBox" style=""><a href="#changeRecoverQuestion" style="color:white;">Es importante que establezca una pregunta de seguridad para que, en caso de que pierda su dispositivo de confianza, pueda acceder a su cuenta.</a></p>');

    }


plainPassWord = 'testtest';

    if( ( plainPassWord == '' || plainPassWord == undefined ) && localStorage.logged == 'true' ){

        getStatus();

        var options = {
            buttons: {
                confirm: {
                    text: 'Hecho',
                    className: 'blue',
                    action: function(e) {

                        

                        if( Aes.Ctr.decrypt( localStorage.password , e.input , 256 ) == e.input ){

                            plainPassWord = e.input;
                            
                            if( localStorage.passwordList != undefined ){
                                
                                jPass = JSON.parse( Aes.Ctr.decrypt( localStorage.passwordList , plainPassWord , 256 ));
                                for( i = 0 ; i < jPass.length ; i++ )
                                    passwordList.push( jPass[i] );

                            }

                            Apprise('close');


                        }else{
                         
                            $("#msg").html("<b style='color:red;'>La contraseña introducida NO es correcta.</b>");
                        
                        }

                    }
                },
            },
            input: true,
        };

        Apprise( '<b>Atención:</b><br /><span id="msg">Para garantizar la seguridad del sistema y no almacenar su contraseña en texto plano, es necesario que en cada sesión introduzca su contraseña.</span>' , options);
        
    }



/*
 * Se añade al comportamiento del ajaxStart y Success la opción
 * de cargar un loader modal para indicar que se están procesando
 * datos usando la librería blockUI
 */

    $( document ).ajaxStart( function() 
    {

        $.blockUI(
        {
            fadeIn: 0, 
            css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px',
                    opacity: .6, 
                    color: '#fff' 
                },
            message: '<img src="jquery/images/ajax-loader.gif" /><br />Cargando'
        });

    });

    $( document ).ajaxSuccess( function( event, xhr, settings )
    {
        $.unblockUI();

    });


$("#waitbutton").click(function(){

    $("body").pagecontainer( "change", "#login-wait" , { transition : 'none' });

});


$("#logout").click(function(){


    localStorage.removeItem('privKey');
    localStorage.removeItem('pubKey');
    localStorage.removeItem('serverPublicKey');
    localStorage.removeItem('uid');
    localStorage.removeItem('logged');
    localStorage.removeItem('name');
    localStorage.removeItem('password');
    localStorage.removeItem('account');
    localStorage.removeItem('id');
    localStorage.removeItem('device');
    localStorage.removeItem("session");
    localStorage.removeItem('selectedServer');
    localStorage.removeItem('dbHash');
    localStorage.removeItem('passwordList');

    document.location.href = "register.html";

});

function showLocalStorageItems(){
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("LocalPriv:"+localStorage.privKey);
    console.log("LocalPub:"+localStorage.pubKey);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("Server:"+localStorage.serverPublicKey);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("Logged: "+localStorage.logged);
    console.log("User Name: "+localStorage.name);
    console.log("LocalPass:"+localStorage.password);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("Account:"+localStorage.account);
    console.log("Session ID:"+localStorage.session);
    console.log("Temp Session: "+localStorage.tmpSession);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("DeviceID: "+localStorage.deviceId);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("Server List: "+localStorage.serverList);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log("Password List: "+localStorage.passwordList);
    console.log("-------------------------------------------");
    console.log("PassList Deco: ( se muestra como string pero es un array ) "+passwordList );

}

showLocalStorageItems();


});