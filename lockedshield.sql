-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-04-2014 a las 22:52:24
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `lockedshield`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lck_credentials`
--

CREATE TABLE IF NOT EXISTS `lck_credentials` (
  `lck_uid` varchar(64) NOT NULL,
  `lck_credentials` varchar(1024) NOT NULL,
  `lck_check` varchar(200) NOT NULL,
  PRIMARY KEY (`lck_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lck_devices`
--

CREATE TABLE IF NOT EXISTS `lck_devices` (
  `lck_uid` varchar(50) NOT NULL,
  `lck_dev` varchar(80) NOT NULL,
  `lck_public` varchar(200) NOT NULL,
  `lck_private` varchar(500) NOT NULL,
  `lck_active` tinyint(1) NOT NULL,
  `lck_sessid` varchar(100) NOT NULL,
  `lck_name` varchar(150) NOT NULL,
  PRIMARY KEY (`lck_uid`,`lck_public`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lck_strongbox`
--

CREATE TABLE IF NOT EXISTS `lck_strongbox` (
  `lck_uid` varchar(50) NOT NULL,
  `lck_ref` varchar(32) NOT NULL,
  `lck_data` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
